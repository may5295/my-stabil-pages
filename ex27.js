let str = "  js string exercises  "
let newstr = str.trim();
function convert (x) {
    let arr = x.split(" ");
    let emp = [];
    for ( let i = 0; i < arr.length; i++) {
        let current = arr[i];
       if(current){
            let letter = current.split('')
            let firstLetter = letter[0].toUpperCase();
            letter.shift();
            let new2 = firstLetter + letter.join('');
            emp.push(new2)
        }
    }
    return emp.join(' ');
}
let result = convert(newstr)
console.log(result);