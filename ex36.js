function curry(fn) {
    return function neu (...x) {
        if (x.length >= 3) {
            return fn(...x);
        }
        else {
            return function (...y) {
                let result = x.concat(y);
                return neu(...result);
            }
        }
    }
}
function addFn(a, b, c) {
    return a + b + c;
}
const add = curry(addFn)
console.log(add(1, 2, 3)); // 6
console.log(add(1, 2)(3)); // 6
console.log(add(1)(2)(3)); // 6