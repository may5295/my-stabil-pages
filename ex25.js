let arr = [1,2,'a','a',1,2,1,1,1,'a'];
let map = new Map();
for( let i = 0; i < arr.length; i++){

    if(map.has(arr[i])){
        let repeat = map.get(arr[i]);
        repeat = repeat + 1;
        map.set(arr[i],repeat)
    } else {
        map.set(arr[i],1)
    }
}

console.log(map)