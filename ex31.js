let obj1 = { a:1, b:0};
let obj2 = { b:0, a:1};
let arr3 = [{ a: 1 }, { b: 2 }]
let arr4 = [{ a: 1 }, { b: 2 }]
function isEqual(x, y) {
    let arr1 = Object.keys(x);
    let arr2 = Object.keys(y);
    // console.log(arr1)
    // console.log(arr2)
    if ( arr1.length !== arr2.length) {
        return false;
    }
    else {
        if (!Array.isArray(x) && !Array.isArray(y)) {
            for (let key in x) {
                if (x[key] !== y[key]) {
                    return false;
                }
            }
        }
        else {
            for ( let j=0; j< x.length; j++) {
                if (typeof x[j] === 'object') {
                    if (!isEqual(x[j],y[j])){
                        return false;
                    }
                }                
            }
        }
        return true;
    }
    
}
console.log(isEqual(arr3, arr4))

let user = {
    name: "Hieu"
}