function compose(...funcs) {
    return function(x) {
        for (let i = funcs.length - 1; i >= 0; i--) {
            funcs[i](x);
        }
    }  
}

function eat(developer) {
    console.log(`${developer.name} is eating`);
    return developer;
}

function code(developer) {
    console.log(`${developer.name} is coding`);
    return developer;
    // developer.alive = false; // rip
}

function sleep(developer) {
    console.log(`${developer.name} is sleeping`);
    return developer;
}

const developer = {
    name: 'Peter',
    alive: true,
};

const live = compose(sleep, code, eat);
// while (developer.alive) {
live(developer);
// }

// Peter is eating
// Peter is coding
// Peter is sleeping