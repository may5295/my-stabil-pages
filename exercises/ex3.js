let array = [1, [2], [3, [[4]]],[5,6]];
function flatten (x,y) {
    let array2 = []
    if (y) {
        return x.flat();
    }
    for (let i = 0; i < x.length; i++) {
        if (!Array.isArray(x[i])) {
            array2.push(x[i]);
        }
        else {
            array2 = array2.concat(flatten(x[i]));
        }
    }
    return array2;
}
console.log(flatten(array,true))