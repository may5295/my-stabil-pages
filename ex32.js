const tree = [
    {
        item: {
            name: 'A'
        },
        children: [
            {
                
                children: [{
                    children: [ {
                        item: {
                            name: 'A-1-2'
                        }
                    }
                    ],
                    item: {
                        name: 'A-1-1'
                    }
                }],
                item: {
                    name: 'A-1'
                }
            }
            , 
            {
                item: {
                    name: 'A-2'
                },
                children: [{
                    children: [{
                        item: {
                            name: 'A-2-2'
                        }
                    } ],
                    item: {
                        name: 'A-2-1'
                    }
                }]
            }
        ]
    }
];
let Arr = []
function flatify (arr) {
    for(let i = 0; i < arr.length; i++){
        // for(let key in arr[i]){
        //     if(Array.isArray(arr[i][key])){
        //        flatify(arr[i][key])
        //     } else {
        //         Arr = Arr.concat(arr[i][key].name)
        //     }
        // }
        Arr = Arr.concat(arr[i].item.name)
        if (arr[i].children) {
            flatify(arr[i].children)
        }
    }
    return Arr;
}
console.log(flatify(tree));